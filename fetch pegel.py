import requests
import datetime
from datetime import timedelta
import pandas as pd
import matplotlib.pyplot as plt



#fetch pegel data
response=requests.get("https://www.pegelonline.wsv.de/webservices/rest-api/v2/stations.json?includeTimeseries=true&includeCurrentMeasurement=true")
#print(response.json())

#append stations names,location names, water name
json_file= response.json()
station_names=[]
water_name=[]
for i in range(len(json_file)):
    station_names.append(json_file[i]["agency"])
    water_name.append(json_file[i]["water"]["longname"])

#station_names=set(station_names)
#water_name=set(water_name)


#this function calls values of related water and the date
##def valuesWater(water_name):
##    properties_water={}
##    for i in range(len(json_file)):
##        if water_name in json_file[i]["water"]["longname"]:
##            timestamp_string=json_file[i]["timeseries"][0]["currentMeasurement"]["timestamp"]
##            water_time = datetime.datetime.fromisoformat(timestamp_string)
##            water_time=str(water_time)
##            water_time=water_time[:18]
##            value=json_file[i]["timeseries"][0]["currentMeasurement"]["value"]
##            value_level=json_file[i]["timeseries"][0]["currentMeasurement"]["stateMnwMhw"]
##            print(water_name,value,water_time)
##
##valuesWater("ALLER")


#convert properties into GeoJSON format
def toGeoJSON_current(lat,lng,station_name,water_name,value,value_level,timestamp):
        {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [lat, lng]
      },
      "properties": {
        "stationname": station_name,
        "watername": water_name,
        "value":value,
        "valuelevel": value_level,
        "timestamp":timestamp

      }
    }
def toGeoJSON_lastday(lat,lng,station_name,water_name,value,timestamp):
        {
      "type": "Feature",
      "geometry": {
        "type": "Point",
        "coordinates": [lat, lng]
      },
      "properties": {
        "stationname": station_name,
        "watername": water_name,
        "attributes":{"value":value,"timestamp":timestamp}

      }
    }


#find the station id by its name
def stationidfinder(name):
    url=f"https://www.pegelonline.wsv.de/webservices/rest-api/v2/stations.json"
    request=requests.get(url)
    response=request.json()
    id=""
    for i in range(len(response)):
        if name==response[i]["longname"]:
            id=response[i]["uuid"]
            latitude=response[i]["latitude"]
            longitude=response[i]["longitude"]
            #print(id)
    return id


#bring last measurement of the water level of given station id
def currentLevel_bystation(name):
    id=stationidfinder(name)[0]
    url=f"https://www.pegelonline.wsv.de/webservices/rest-api/v2/stations/{id}/W/currentmeasurement.json"
    request=requests.get(url)
    response=request.json()

    #export time and date
    time=datetime.datetime.fromisoformat(response["timestamp"])
    time=str(time)
    time=time[:18]

    #export level and how the minimum value(low,normal or high)
    value=response["value"]
    level=response["stateMnwMhw"]

    return (name,time,value,level)



#this function brings values of the last months values
def onemonthdata(id):
    current_time=datetime.datetime.now()
    onemonthduration=current_time-timedelta(30)
    current_time=current_time.isoformat()
    onemonthduration=onemonthduration.isoformat()

    url=f"https://www.pegelonline.wsv.de/webservices/rest-api/v2/stations/{id}/W/measurements.json?start={onemonthduration}&end={current_time}"
    value=[]
    time_stamp=[]
    request=requests.get(url)
    response=request.json()

    curr=datetime.datetime.now()
    curr2=datetime.datetime.isoformat(curr)

    timestamp=response[-1]["timestamp"]
    time_nformat=datetime.datetime.fromisoformat(timestamp)

    for i in range(len(response)):
        for k in range(30):
            time_def=time_nformat-timedelta(k)
            time_iso=datetime.datetime.isoformat(time_def)
            if time_iso==response[i]["timestamp"]:
                #print(response[i]["value"])
                value.append(response[i]["value"])
                time_stamp.append(response[i]["timestamp"])


    return value,time_stamp


#this function finds stations from water name
def findStationsfromwatername(name):
    url="https://www.pegelonline.wsv.de/webservices/rest-api/v2/waters.json?includeStations=true"
    request=requests.get(url)
    response=request.json()
    st=[]
    for i in range(len(response)):
        if response[i]["longname"]==name:
            stations=response[i]["stations"]
            for j in stations:
                #print(j["longname"])
                st.append(j["longname"])
    return st



#userinput=str(input(print("search by station or water?")))

userinput="water"
values=[]
time_stamp=[]
station_values=[]
if userinput=="water":
    #water_name=input(print("Enter the water name"))
    water_name="ALLER"
    stations_=findStationsfromwatername(water_name)

    for i in stations_:

        id=stationidfinder(i)

        values.append(onemonthdata(id)[0])
        time_stamp.append(onemonthdata(id)[1])




#gg

elif userinput=="station":print("gg")

plt.plot(values,time_stamp)
