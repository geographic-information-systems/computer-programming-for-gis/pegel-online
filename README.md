# Pegel Online



## Getting started

- We are going to create QGIS plugins by utilizing PEGEL Online
- Pegel online provides data which water level measurements of rivers in Germany
- The dataset include time series, therefore there are many data in every date
- The dataset is stored as json format. Keys:
    ["uuid", "number","shortname","longname","km","agency","longitute", "latitude","water":{"shortname","longname"}]
- The steps can be seen following sections:
    1- Convert json file to geojson format
        - Import the json data: station.json
        - Convert to geojson by using conver2geojson function
	  - Writing the geojson to a file as an output
        




```
cd existing_repo
git remote add origin https://gitlab.com/geographic-information-systems/computer-programming-for-gis/pegel-online.git
git branch -M main
git push -uf origin main
```

